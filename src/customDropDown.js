import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
  Animated,
  NativeModules,
  LayoutAnimation,
  Image,
  TextInput
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import FontAwesome, { Icons } from "react-native-fontawesome";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";

// const data = new Array(100)
//     .fill(0)
//     .map((a, i) => ({ key: i, value: 'item' + (i + 100) }));

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const { height, width } = Dimensions.get("window");
export default class CustomDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      dropDownX: 0,
      dropDownX: 0,
      textFieldWidth: 0,
      selectedDropDown: null,
      orignalList: [],
      data: []
    };
    this.isFloatingTextHidden = true;
  }

  dropDownTapped = () => {
    this.refs.mainView.measureInWindow((x, y, width, height) => {
      // console.log('Component width is: ' + width)
      // console.log('Component height is: ' + height)
      // console.log('X offset to frame: ' + fx)
      // console.log('Y offset to frame: ' + fy)
      // console.log('X offset to page: ' + px)
      // console.log('Y offset to page: ' + py)
      this.updateDropDownLayout(this.props.direction, {
        x: x,
        y: y,
        width: width,
        height: height
      });
      this.setState({ modalVisible: !this.state.modalVisible });
    });
    // console.log(this)
  };

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.selectedIndex < nextProps.data.length) {
      this.setState({ selectedDropDown: nextProps.selectedDropDown });
      // this.setState({selectedDropDown: nextProps.data[nextProps.selectedIndex]})
    }
    this.state.orignalList = [...nextProps.data]
    this.state.data = [...nextProps.data]
  }

  containerOnLayout = event => {
    // const frame = event.nativeEvent.layout;
    // this.refs.mainView.measureInWindow( (fx, fy, width, height, px, py) => {
    //     })
    // console.log(frame)
  };

  backgroundButtonTapped = () => {
    this.hidePopover();
  };

  predictDirection = (y, totalHeight) => {
    if (totalHeight + 10 > height) {
      return "top";
    } else {
      return "bottom";
    }
  };

  updateDropDownLayout = (direction, frame) => {
    const offset = 5;
    if (direction == "auto") {
      direction = this.predictDirection(
        frame.y,
        this.props.dropDownHeight + frame.y + frame.height
      );
    }
    switch (direction) {
      case "top":
        this.setState({
          dropDownX: frame.x,
          dropDownY: frame.y - this.props.dropDownHeight - offset
        });
        break;
      case "bottom":
        this.setState({
          dropDownX: frame.x,
          dropDownY: frame.y + frame.height + offset
        });
        break;
      case "left":
        break;
      case "right":
        break;
      default:
        break;
    }
    this.setState({ textFieldWidth: frame.width });
  };

  hidePopover = () => {
    this.setState({ modalVisible: false });
  };

  dropDownSelected = item => {
    this.setState({ selectedDropDown: item });
    this.props.onValueSelected(item);
    this.hidePopover();
  };

  get isItemSelected() {
    return (
      this.state.selectedDropDown != null &&
      this.state.selectedDropDown.value != null &&
      this.state.selectedDropDown.value.length > 0
    );
  }

  get text_PlaceHolderStyle() {
    if (this.isItemSelected) {
      return [this.props.dropDownFieldTextStyle]
    }
    else {
      return [this.props.dropDownFieldTextStyle, {color: this.props.placeHolderColor}]
    }
  }

  get getSelectedValue() {
    if (this.isItemSelected) {
      const tmpValue = this.state.selectedDropDown.value;
      return this.state.selectedDropDown.value;
    } else {
      return this.props.placeHolder;
    }
  }

  getFlatlist = () => {
    if (this.state.modalVisible) {
      return (
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {}}
        >
          <View>
            <TouchableOpacity
              activeOpacity={1.0}
              onPress={this.backgroundButtonTapped}
              style={{
                position: "absolute",
                width: width,
                height: height,
                backgroundColor: "transparent"
              }}
            />

            <LinearGradient
              startPoint={{ x: 0.0, y: 1.0 }}
              endPoint={{ x: 0.0, y: 0.0 }}
              colors={this.props.backgroundColors}
              style={[
                {
                  height: this.props.dropDownHeight,
                  width: this.state.textFieldWidth,
                  borderRadius: 10.0,
                  marginTop: this.state.dropDownY,
                  marginLeft: this.state.dropDownX
                },
                this.props.listStyle
              ]}
            >
              <TextInput
              style={[{height: 35, marginTop: 5, marginHorizontal: 8, borderRadius: 5, color: 'black', backgroundColor: 'white'}, ...this.props.searchFieldStyle]}
              onChangeText={ (text) => {
                const filteredList = this.state.orignalList.filter( item => _.includes(item.value, text) || !text.length )
                this.setState({data: filteredList})
               }}
              {...this.props.searchFieldProps}
               />
              <FlatList
                style={[{ backgroundColor: "transparent" }, this.flatListStyle]}
                onLayout={this.flatListOnLayout}
                keyExtractor={item => item.value}
                data={this.state.data}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.dropDownSelected(item);
                      }}
                      style={[
                        { paddingVertical: 5, paddingHorizontal: 10 },
                        this.listCellStyle
                      ]}
                    >
                      <Text
                        style={[
                          {
                            color: "black",
                            alignSelf: "flex-start",
                            marginBottom: 5
                          },
                          this.props.listTextStyle
                        ]}
                      >
                        {item.value}{" "}
                      </Text>
                      {this.props.hasLineSeperator && (
                        <View
                          style={[
                            { borderBottomWidth: 0.5, borderColor: "white" },
                            this.props.lineSeperatorStyle
                          ]}
                        />
                      )}
                    </TouchableOpacity>
                  );
                }}
              />
              {/* </View> */}
            </LinearGradient>
          </View>
        </Modal>
      );
    } else {
      return null;
    }
  };

  get getArrowImage() {
    if (this.props.fontAwesomeIcon != null) {
      // return (

      //     <Image style={{ alignSelf: 'center' }} source={this.props.imagePath} />
      // );

      return (
        <FontAwesome
          {...this.props.dropDownFontAwesomeProp}
          style={[{ alignSelf: "center", fontSize: 18 }]}
        >
          {this.props.fontAwesomeIcon}
        </FontAwesome>
      );
    } else {
      return null;
    }
  }

  get getFloatingTitle() {
    if (this.isItemSelected) {
      if (this.isFloatingTextHidden) {
        LayoutAnimation.spring();
        this.isFloatingTextHidden = false;
      }

      let title = null;
      if (this.props.floatingText != null) {
        title = this.props.floatingText;
      } else if (this.props.placeHolder != null) {
        title = this.props.placeHolder;
      }

      return <Text style={[this.props.floatingTextStyle]}>{title}</Text>;
    } else {
      if (!this.isFloatingTextHidden) {
        LayoutAnimation.spring();
        this.isFloatingTextHidden = true;
      }
      return null;
    }
  }

  render() {
    return (
      <View
        ref="mainView"
        onLayout={this.containerOnLayout}
        style={[
          { flex: 0, backgroundColor: "transparent" },
          this.props.mainStyle
        ]}
      >
        {this.getFloatingTitle}

        <View style = {[{ 
          backgroundColor : "white",height: responsiveHeight(7.3), borderTopWidth: 1.0, borderTopColor: 'gray',
          borderBottomWidth: 1.0, borderBottomColor: 'gray' , flexDirection : "row" ,paddingHorizontal: 25}]} >
        

           <Image

            style=  {[{alignSelf:"center", marginRight : responsiveWidth(2.5), height: 15, width : 15}]}
            resizeMode={"center"}
            source={this.props.textIcon}/>

        <TouchableOpacity
          onLayout={this.dropDownFieldOnLayout}
          onPress={this.dropDownTapped}
          style={[
            {
              flexDirection: "row",
              flex:1,
              justifyContent: "space-between",
            }
          ]}
        >
          <Text style={[this.text_PlaceHolderStyle,{}]}>
            {this.getSelectedValue}
          </Text>
          {this.getArrowImage}
        </TouchableOpacity>

        </View>

        {this.getFlatlist()}
      </View>
    );
  }
}

CustomDropDown.defaultProps = {
  direction: "bottom",
  dropDownHeight: 200,
  searchFieldStyle: PropTypes.object,
  searchFieldProps: PropTypes.object,
  floatingTextStyle: PropTypes.object,
  mainStyle: PropTypes.object,
  listStyle: {},
  listTextStyle: PropTypes.object,
  listCellStyle: PropTypes.object,
  dropDownFieldTextStyle: PropTypes.object,
  flatListStyle: PropTypes.object,
  backgroundColors: PropTypes.array,
  hasLineSeperator: true,
  lineSeperatorStyle: PropTypes.object,
  //Data Fields
  data: [],
  selectedIndex: -1,
  imagePath: null,
  floatingText: null,
  onValueSelected: PropTypes.func.isRequired,
  placeHolder: "Select value",
  selectedDropDown: PropTypes.object,
  dropDownFontAwesomeProp: PropTypes.object,
  fontAwesomeIcon: PropTypes.object,
  textIcon : PropTypes.object
};
